import cv2
import json
import os

FROM_PATH_CUT_VIDEO = "../data/test_video_csv/video_cut/2_Mira/20230327_200-400/281-1.mp4"
TO_PATH_JSON_CUT_VIDEO_FOLDER = "../data/test_video_csv/json_video_cut/2_Mira/20230327_200-400/"

filename_json = os.path.split(FROM_PATH_CUT_VIDEO)[-1] + ".json"
path_json = TO_PATH_JSON_CUT_VIDEO_FOLDER + filename_json
with open(path_json, 'r') as outfile:
    dict_json = json.load(outfile)

print("dict_json", dict_json)
print("dict_json", dict_json["tags"][0]["frame_range"])

frame_range = dict_json["tags"][0]["frame_range"]
cap = cv2.VideoCapture(FROM_PATH_CUT_VIDEO)
frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
fps = cap.get(cv2.CAP_PROP_FPS)
fc = 0
while True:
    ret, frame = cap.read()
    if not ret:
        break
    if fc<frame_range[0] or frame_range[1]<fc:
        # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame = cv2.convertScaleAbs(frame, alpha=0.5, beta=0)
    cv2.imshow("frame", frame)
    # if cv2.waitKey(1) & 0xFF == ord('q'):
    #     break
    key = cv2.waitKey(int(1000/fps)) & 0xFF
    if key == ord('q'):
        break

    fc = fc + 1
cap.release()
cv2.destroyAllWindows()