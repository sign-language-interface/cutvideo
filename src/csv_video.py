import cv2
import os
import glob
import csv

import json



FROM_GLOB_VIDEO = "../data/test_video_csv/video_raw/2_Mira/20230327_200-400/*.MTS"
FROM_GLOB_CSV = "../data/test_video_csv/adobe_pp_2021/2_Mira/20230327_200-400/*.csv"
TO_PATH_CUT_VIDEO_FOLDER = "../data/test_video_csv/video_cut/2_Mira/20230327_200-400/"
TO_PATH_JSON_CUT_VIDEO_FOLDER = "../data/test_video_csv/json_video_cut/2_Mira/20230327_200-400/"


SECOND_TO_CUT = 2
TEST_WITHOUT_CUT = False


def read_csv(path_csv):
    print("path_csv", path_csv)
    list_ret = []
    list_fps = []

    with open(path_csv) as csvfile:
        # работает только python 3.10.2 или больше, а не работает только python 3.8.10
        spamreader = csv.reader(csvfile, delimiter='\t')
        i = 2
        for row in spamreader:
            # print("row", row)
            # print("len(row)", len(row))
            if len(row) > 1:
                # print("row[2]", type(row[2].encode('utf-8')))
                # print("i", i)
                d = row[2]
                # print("d", d)
                d = row[2].encode('utf-16').decode('utf-16').replace('\x00', '')
                s = row[0]
                s = row[0].encode('utf-16').decode('utf-16').replace('\x00', '')
                s = s.split(",")
                if len(s) > 1:
                      fps = d.split(":")[-1]
                      # print("fps", fps)
                      list_fps.append(fps)
                      list_ret.append([i, s[0], s[1], d])
                      i += 1
        print("max(list_fps)", max(list_fps))
    list_ret_range = []
    list_range = []
    for n in list_ret:
        if len(list_range)==0 and n[2] == "SignsBegin":
            list_range.append(n[0])
            list_range.append(n[1])
            list_range.append(n[3])
        else:
            list_range.append(n[3])
            list_ret_range.append(list_range)
            list_range = []
    # print("list_ret_range", list_ret_range)
    return list_ret_range


def add_time(time1, time2, fps):
    h1, m1, s1, f1 = map(int, time1.split(':'))
    h2, m2, s2, f2 = map(int, time2.split(':'))

    frames = f1 + f2
    seconds = s1 + s2
    minutes = m1 + m2
    hours = h1 + h2

    if frames >= fps:
        frames -= fps
        seconds += 1

    if seconds >= 60:
        seconds -= 60
        minutes += 1

    if minutes >= 60:
        minutes -= 60
        hours += 1

    return '{:02d}:{:02d}:{:02d}:{:02d}'.format(hours, minutes, seconds, frames)



def subtract_time(time1, time2, fps=50):
    h1, m1, s1, f1 = map(int, time1.split(':'))
    h2, m2, s2, f2 = map(int, time2.split(':'))
    
    frames = f1 - f2
    if frames < 0:
        frames += fps
        s1 -= 1
    
    seconds = s1 - s2
    if seconds < 0:
        seconds += 60
        m1 -= 1
    
    minutes = m1 - m2
    if minutes < 0:
        minutes += 60
        h1 -= 1
    
    hours = h1 - h2
    
    return '{:02d}:{:02d}:{:02d}:{:02d}'.format(hours, minutes, seconds, frames)

def timecode_to_frames(timecode, fps):
    hours, minutes, seconds, frames = map(int, timecode.split(':'))
    total_seconds = hours * 3600 + minutes * 60 + seconds
    total_frames = total_seconds * fps + frames
    return total_frames

def timecode_to_timecode_round(timecode, fps):
    h, m, s, f = map(int, timecode.split(':'))
    assert fps>=f # Не должен быть меньше f, чем fps
    # print("f/fps", f/fps)
    return '{:02d}:{:02d}:{:02d}.{}'.format(int(h), int(m), int(s), str(round((f/fps), 3)).replace('0.', ''))


def timecode_to_second(timecode, fps):
    return int(timecode.split(":")[0]) * 60 * 60 + int(timecode.split(":")[1]) * 60 + int(timecode.split(":")[2]) + int(timecode.split(":")[3]) / fps
# r = timecode_to_second("00:00:00:25", 50)
# print(r)

def second_to_timecode(time_in_seconds, fps):
    total_frames = round(time_in_seconds * fps)
    frames = total_frames % fps
    total_seconds = total_frames // fps
    seconds = total_seconds % 60
    total_minutes = total_seconds // 60
    minutes = total_minutes % 60
    hours = total_minutes // 60
    return "{:02d}:{:02d}:{:02d}:{:02d}".format(hours, minutes, seconds, frames)
    
# r = second_to_timecode(100, 50)
# print(r)


import sys
import datetime


def cut_video_between_signs_json(from_glob_video, from_glob_csv, to_path_cut_video_folder, file_format = "mp4"):
    list_path_video = glob.glob(from_glob_video)
    list_path_csv = glob.glob(from_glob_csv)
    
    print("list_path_video: ", list_path_video)
    print("list_path_csv: ", list_path_csv)
    

    for path_video in list_path_video:

        path_log = path_video + '.log'
        if os.path.exists(path_log):
            os.remove(path_log)
        with open(path_log, 'a') as f:
            if not TEST_WITHOUT_CUT:
                sys.stdout = f
            print("datatime_begin", str(datetime.datetime.now()) + "------------------------------------------------------")
            cap = cv2.VideoCapture(path_video)
            fps = int(round(cap.get(cv2.CAP_PROP_FPS), 0))
            frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
            width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))

            print("frame_count", frame_count)

            for path_csv in list_path_csv:
                if os.path.split(path_video)[1].split(".")[0] == os.path.split(path_csv)[1].split(".")[0]:
                    print(path_video, path_csv)
                    list_marker = read_csv(path_csv)
                    len_list_marker = len(list_marker)
                    list_marker_cut = []
                    len_list_marker = len(list_marker)
                    print("len(list_marker)", len_list_marker)
                    for i, n in enumerate(list_marker):
                        print("list_marker------------------------------------------------------")
                        print("n", n)
                        if len_list_marker==1:
                            # FIXME
                            last_time = frames_to_timecode(frame_count, fps=fps)
                            print("last_time", last_time)
                            # ave_time_above = average_time('00:00:00:00', n[2], fps=fps) # ave_time_above 00:01:09:09
                            ave_time_above = subtract_time(n[2], second_to_timecode(SECOND_TO_CUT, fps=fps), fps=fps)
                            # ave_time_below = average_time(n[3], last_time, fps=fps) # ave_time_below 00:06:27:21
                            ave_time_below = add_time(n[3], second_to_timecode(SECOND_TO_CUT, fps=fps), fps=fps)
                            print("ave_time_above", ave_time_above)
                            print("ave_time_below", ave_time_below)
                            r = [n[0], n[1], ave_time_above, ave_time_below]
                            list_marker_cut.append(r)
                            print("len_list_marker==1", r)
                            continue

                        if i==0:
                            ave_time_above = subtract_time(n[2], second_to_timecode(SECOND_TO_CUT, fps=fps), fps=fps)
                            ave_time_below = add_time(n[3], second_to_timecode(SECOND_TO_CUT, fps=fps), fps=fps)
                            r = [n[0], n[1], ave_time_above, ave_time_below]
                            list_marker_cut.append(r)
                            print("first_n", r)
                            continue

                        if i==len_list_marker-1:
                            last_time = frames_to_timecode(frame_count, fps=fps)
                            print("last_time", last_time)
                            ave_time_above = subtract_time(n[2], second_to_timecode(SECOND_TO_CUT, fps=fps), fps=fps)
                            ave_time_below = add_time(n[3], second_to_timecode(SECOND_TO_CUT, fps=fps), fps=fps)
                            r = [n[0], n[1], ave_time_above, ave_time_below]
                            list_marker_cut.append(r)
                            print("last_n", r)
                            continue

                        ave_time_above = subtract_time(n[2], second_to_timecode(SECOND_TO_CUT, fps=fps), fps=fps)
                        ave_time_below = add_time(n[3], second_to_timecode(SECOND_TO_CUT, fps=fps), fps=fps)
                        r = [n[0], n[1], ave_time_above, ave_time_below]
                        list_marker_cut.append(r)
                        print("current_n", r)

                    for i, n in enumerate(list_marker_cut):
                        print("list_marker_cut ------------------------------------------------------")
                        print(n)
                        result_timecode = subtract_time(n[3], n[2], fps=fps)
                        
                        timecode_round = timecode_to_timecode_round(n[2], fps=fps)
                        print("new_timecode", n[2], "->", timecode_round)

                        result_frames = timecode_to_frames(result_timecode, fps=fps)
                        
                        to_path_cut_video = os.path.join(to_path_cut_video_folder, n[1] + "." + file_format)

                        # print("timecode_round", timecode_round, "result_frames", result_frames)

                        command = f"ffmpeg -ss {timecode_round} -i {path_video} -c:v libx264 -c:a aac -frames:v {result_frames} {to_path_cut_video}"
                        print(command)
                        if not TEST_WITHOUT_CUT:
                            p = os.system(command)
                        # print("return from ffmpeg-------------------------------------------------------------------------")
                        # print(p)

                    dict_ret = {"version":"0.1"}
                    for s, c in zip(list_marker, list_marker_cut):
                        print("json ------------------------------------------------------")
                        print("o:", s, "c:", c)
                        m1_timecode = subtract_time(s[2], c[2], fps=fps)
                        m2_timecode = subtract_time(s[3], c[2], fps=fps)
                        video_length_timecode_clip = subtract_time(c[3], c[2], fps=fps)
                        frame_count_clip = timecode_to_frames(video_length_timecode_clip, fps=fps)
                        timecode_range = [m1_timecode, m2_timecode]
                        frame_range = [timecode_to_frames(m1_timecode, fps), timecode_to_frames(m2_timecode, fps)]

                        # fps = int(round(cap.get(cv2.CAP_PROP_FPS), 0))
                        # frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
                        # height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
                        # width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))

                        dict_ret["size"] = {"height": height, "width": width}
                        dict_ret["fps"] = fps
                        dict_ret["video_length_timecode"] = video_length_timecode_clip
                        dict_ret["frame_count"] = frame_count_clip

                        dict_ret["tags"] = []
                        dict_tag = {}
                        dict_tag["name"] = "signs"
                        dict_tag["timecode_range"] = timecode_range
                        dict_tag["frame_range"] = frame_range
                        dict_ret["tags"].append(dict_tag)                  

                        dict_ret["from_path_video"] = os.path.normpath(path_video).replace('\\', '/')
                        dict_ret["from_path_csv"] = path_csv.replace('\\', '/')


                        json_object = json.dumps(dict_ret, indent=4)
                        filename_json = f"{s[1]}.{file_format}.json"
                        path_json = os.path.join(TO_PATH_JSON_CUT_VIDEO_FOLDER, filename_json)
                        print(f"Written to file: {path_json}")
                        print(dict_ret)
                        if not TEST_WITHOUT_CUT:
                            with open(path_json, "w") as outfile:
                                outfile.write(json_object)

            print("datatime_end", str(datetime.datetime.now()) + "------------------------------------------------------")
            if not TEST_WITHOUT_CUT:
                sys.stdout = sys.__stdout__

def average_time(time1, time2, fps):
    sub = subtract_time(time2, time1, fps)
    # print("sub", sub)
    
    ave = find_half_time(sub, fps)
    # print("ave", ave)
    
    return subtract_time(time2, ave, fps)


def average_time_last(time1, time2, fps, max_second=2):
    # FIXME
    sub = subtract_time(time2, time1, fps)
    print("sub", sub)
    ave = find_half_time(sub, fps)
    
    print("ave", ave)
    print("timecode_to_second(ave, fps)", timecode_to_second(ave, fps))
    timecode_to_sec = timecode_to_second(ave, fps)
    if timecode_to_sec>=max_second:
        ave = second_to_timecode(2,fps)
    print("ave", ave)

    return subtract_time(time2, ave, fps)


def timecode_to_second(timecode, fps=25):
    h, m, s, f = map(int, timecode.split(':'))
    total_frames = ((h * 3600) + (m * 60) + s) * fps + f
    return round(total_frames / fps, 2)


def find_half_time(time, fps=50):
    h, m, s, f = map(int, time.split(':'))
    total_frames = h * 60 * 60 * fps + m * 60 * fps + s * fps + f
    half_frames = total_frames // 2
    h, remaining_frames = divmod(half_frames, 60 * 60 * fps)
    m, remaining_frames = divmod(remaining_frames, 60 * fps)
    s, f = divmod(remaining_frames, fps)
    return '{:02d}:{:02d}:{:02d}:{:02d}'.format(h, m, s, f)
    

def frames_to_timecode(frames, fps):
    total_seconds = frames / fps
    hours, seconds = divmod(total_seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    frames = int(round((seconds - int(seconds)) * fps))
    return '{:02d}:{:02d}:{:02d}:{:02d}'.format(int(hours), int(minutes), int(seconds), frames)


if __name__ == "__main__":
    cut_video_between_signs_json(FROM_GLOB_VIDEO, FROM_GLOB_CSV, TO_PATH_CUT_VIDEO_FOLDER)
